'use strict';

const path = require('path');

module.exports = {
    entry: {
        main: path.resolve('./src/main.jsx')
    },

    output: {
        filename: '[name].bundle.js',
        path: path.resolve('./public/dist')
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                      loader: 'babel-loader',
                      options: {
                        presets: ['react']
                      }
                    }
                  ],
            }
        ]
    }
};
