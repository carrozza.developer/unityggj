import React from 'react';
import Unity, { UnityContent } from "react-unity-webgl";


const UnityComms = {
  send(message) {
    this.unityContent.send("UnityComms", "OnMessageToUnity", JSON.stringify(message))
  }
}


class UnityGame extends React.Component {
  constructor(props) {
    super(props);
    this.state = {  }
    this.unityContent = new UnityContent(
      "Build/unity_game.json",
      "Build/UnityLoader.js"
    );
    UnityComms.send = UnityComms.send.bind(this);
    this.unityContent.on("loaded", () => {
      console.log("Loaded");
      UnityComms.send(this.props.userData);
    })
  }
  
  render() { 
    return ( <Unity  unityContent={this.unityContent}/>);
  }
}
 
export { UnityGame, UnityComms};