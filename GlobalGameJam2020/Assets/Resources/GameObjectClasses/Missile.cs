using System.Collections.Generic;
using UnityEngine;

public class Missile : WorldObject
{
    public string ClientId;
    public bool Exploded;

    public void UpdateLocation(Vector3 location) {
      transform.position = location;
    }
}


// public class Missiles : List<Missile>
// {
//     public MonoBehaviour Add(string clientId, Vector3 location) {
//         return this.Add(new Missile() { ClientId = clientId, Location = location });
//     }
// }