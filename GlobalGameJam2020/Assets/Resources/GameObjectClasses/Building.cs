using System.Collections.Generic;
using UnityEngine;

public enum BuildingType {
  Normal = 0
}

public class Building : WorldObject
{
    public static Building Create(BuildingType type, Vector3 location, Vector3 rotation)
    {
        var b = new Building();
        b.Type = type;
        b.Location = location;
        b.Rotation = rotation;
        b.Health = 100;

        return b;
    }

    public BuildingType Type = BuildingType.Normal;
    public Vector3 Rotation = new Vector3();
    public decimal Health = 100;
}

public class Buildings : List<Building>
{

}