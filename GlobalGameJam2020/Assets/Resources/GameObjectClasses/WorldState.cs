using System.Collections.Generic;
using UnityEngine;

public abstract class WorldObject : MonoBehaviour
{
    public Vector3 Location = new Vector3();

    private Vector3 startLocation;
    private Quaternion startRotation;

    void Start()
    {
        startLocation = transform.position;
        startRotation = transform.rotation;
    }

    public void resetPosition()
    {
        transform.position = startLocation;
        transform.rotation = startRotation;
    }
}

public class WorldState
{
    public Player Player { get; private set; }
    public List<GameObject> Buildings = 
      new List<GameObject>();
    public Dictionary<string, GameObject> Missiles = 
      new Dictionary<string, GameObject>();

    public string ServerId;
    public string JoinURI;
}
