using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchTest : MonoBehaviour
{
    public StateManager sm;
    private int nextClientId = 0;

    // Start is called before the first frame update
    void Start()
    {
        sm = GetComponent<StateManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
          sm.AddClient("client" + nextClientId);
          
          Debug.Log("Created client " + nextClientId);

          nextClientId++;
        }
    }
}