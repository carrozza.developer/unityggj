﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Runtime.InteropServices;

public struct UpdateData
{
    public string clientId;
    public float x;
    public float y;
}

[System.Serializable]
public class MissilesDataList : List<MissleStateData>
{

}

[System.Serializable]
public class MissleStateData
{
    public string clientId;
    public Vector3 position;
    public Vector3 rotation;
    public bool exploded;
}

[System.Serializable]
public class WorldStateData
{
    public Player player;
    public List<MissleStateData> MissleData = new List<MissleStateData>();

    public WorldStateData(Player p)
    {
        player = p;
    }
}

public class StateManager : MonoBehaviour
{
    public WorldState worldState = new WorldState();
    public GameObject missileGameObject;
    Transform playerOne;
    private static readonly float MIN_X = -10.00f;
    private static readonly float MAX_X =  10.00f;
    private static readonly float MIN_Z = -10.00f;
    private static readonly float MAX_Z =  10.00f;
    public AudioClip warning;
    private int sendCount = 0;

    private List<MissleStateData> missileDataList = new List<MissleStateData>();
    [DllImport("__Internal")]
    private static extern void SendWorldStateToJS(string str);

    // Start is called before the first frame update
    void Start()
    {
        playerOne = transform.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {

        //var worldState = WorldState.Collect(); 

        // foreach (var deviceId in instance.GetControllerDeviceIds()) {
        //     instance.Message(deviceId, worldState);    
        // }

        sendCount += 1;
        if (sendCount == 3)
        {
            sendCount = 0;
            SendState();
        }
    }

    private Vector3 getRandomMissileStartLocation() {
        float x = Random.Range(MIN_X, MAX_X);
        float y = 100.00f;
        float z = Random.Range(MIN_Z, MAX_Z);

        return new Vector3(x, y, z);
    }

    public void AddClient(string clientId) {
        // pick a random start point
        var startLocation = this.getRandomMissileStartLocation();
        Debug.Log("Using new vector: " + startLocation);

        // create the missle
        GameObject go = Instantiate(missileGameObject, startLocation, missileGameObject.transform.rotation);
        Debug.Log("GameObject created");
        go.transform.name = "Missile";
        go.transform.GetComponent<Missile>().ClientId = clientId;
        go.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        go.GetComponent<MissleNavigator>().readyFall = true;
        Debug.Log("Instantiated GameObject: " + go);

        // ad the missle to the list
        worldState.Missiles.Add(clientId, go);
        playerOne.GetComponent<AudioSource>().PlayOneShot(warning);
        Debug.Log("Missiles: " + worldState.Missiles.Count);
    }
    public void FallWhenReady(bool fall) {
        MissleNavigator.canFall = fall;
        /*foreach (KeyValuePair<string, GameObject> entry in worldState.Missiles)
        {
            entry.Value.GetComponent<MissleNavigator>().canFall = fall;
        }*/
     }
    public void ResetClient(string clientId)
    {
        if (worldState.Missiles.ContainsKey(clientId))
        {
            GameObject go = worldState.Missiles[clientId];
            if (go != null)
            {
                RemoveClient(clientId);
            }
            worldState.Missiles.Remove(clientId);
        }
        AddClient(clientId);
    }

    public void RemoveClient(string clientId)
    {
        if (worldState.Missiles.ContainsKey(clientId))
        {
            GameObject go = worldState.Missiles[clientId];
            if (go != null) {
                Destroy(go);
                worldState.Missiles.Remove(clientId);
                Debug.Log("Missiles: " + worldState.Missiles.Count);
                return;
            }
            worldState.Missiles.Remove(clientId);
        }
        Debug.Log("missle " + clientId + " was already removed?");
    }

    public void UpdateClient(string message)
    {
        UpdateData msgData = JsonUtility.FromJson<UpdateData>(message);
        string clientId = msgData.clientId;
        if (worldState.Missiles.ContainsKey(clientId))
        {
            GameObject go = worldState.Missiles[clientId];
            if (go != null)
            {
                MissleNavigator missle = go.GetComponent<MissleNavigator>();
                missle.xInput = msgData.x;
                missle.yInput = msgData.y;
                Debug.Log("Updating Missle");
                return;
            }
            worldState.Missiles.Remove(clientId);
        }
        Debug.Log("missle " + clientId + " was already removed?");
    }

    public void SendState()
	{
        string statejson = "{\"missles\":[";
        bool hasmissles = false;
        foreach (KeyValuePair<string, GameObject> entry in worldState.Missiles)
        {
            if (entry.Value != null)
            {
                Vector3 pos = entry.Value.transform.position;
                Vector3 rot = entry.Value.transform.eulerAngles;
                MissleStateData md = new MissleStateData();
                md.clientId = entry.Key;
                md.position = pos;
                md.rotation = rot;
                Missile missile = entry.Value.GetComponent<Missile>();
                md.exploded = missile.Exploded;
                string mdjson = JsonUtility.ToJson(md);
                //Debug.Log("missle json: " + mdjson);
                statejson += mdjson + ",";
                hasmissles = true;
            }
            else
            {
                worldState.Missiles.Remove(entry.Key);
            }
        }
        if (hasmissles)
        {
            statejson = statejson.Substring(0, statejson.Length - 1);
        }
        statejson += "]}";
        //Debug.Log("world state: " + statejson);
        SendWorldStateToJS(statejson);

    }

    public void subscribe(GameObject reference)
    {
        // stateObjects.Add(reference);

    }

    private void UpdateState()
    {
        // stateObjects[0].GetComponent<Pyramid>().getStateInfo();
    }
}
