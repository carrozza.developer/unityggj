﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Generator : MonoBehaviour
{

    public float health = 100;
    public float ouchRate = 50;
    public Image healthBar;

   private Transform player;
    private void Start()
    {
        player = Camera.main.transform;
    }


    private void Update()
    {
        healthBar.fillAmount = health / 100;
        healthBar.transform.parent.transform.LookAt(player.position);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MainCamera"))
        {
            other.GetComponent<PlayerAction>().currentGenerator = this;
        } else if (other.CompareTag("target"))
        {
            health = health - ouchRate;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MainCamera"))
        {

            other.GetComponent<PlayerAction>().currentGenerator = null;
        }
    }

}
