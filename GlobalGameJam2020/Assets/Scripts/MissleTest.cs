﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissleTest : MonoBehaviour
{
    public Vector3 missleLocation;
    public MissileController mc;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mc.UpdateLocation(JsonUtility.ToJson(missleLocation));
    }
}
