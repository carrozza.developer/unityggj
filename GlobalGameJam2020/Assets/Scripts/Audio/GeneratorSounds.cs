﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorSounds : MonoBehaviour
{

    AudioSource m_MyAudioSource;
    Generator gen;
    public AudioClip[] clips;


    // Start is called before the first frame update
    void Start()
    {
        m_MyAudioSource = GetComponent<AudioSource>();
        gen = GetComponentInChildren<Generator>();
    }

    // Update is called once per frame
    void Update()
    {
        float health = gen.health;
        if (health >= 80 )
        {
            if (m_MyAudioSource.clip != clips[0])
            m_MyAudioSource.clip = clips[0];
        }
        else if (health >= 60)
        {
            if (m_MyAudioSource.clip != clips[1])
                m_MyAudioSource.clip = clips[1];
        }
        else if (health >= 40)
        {
            if (m_MyAudioSource.clip != clips[2])
                m_MyAudioSource.clip = clips[2];
        }
        else if (health >= 20)
        {
            if (m_MyAudioSource.clip != clips[3])
                m_MyAudioSource.clip = clips[3];
        }
        else if (health >= 10)
        {
            if (m_MyAudioSource.clip != clips[4])
                m_MyAudioSource.clip = clips[4];
        }
    }
}
