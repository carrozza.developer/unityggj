﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLocationData
{
    public Vector3 position;
    public Vector3 rotation;
}

public class MissileController : MonoBehaviour
{
    // Public function for Mille controller
    // From javsript unityContent.SendMessage("Missle", "UpdateLocation" , "stringy vector3");
    public void UpdateLocation(string message)
    {
        Debug.Log("Updating Client Rocket");
        MissileLocationData data = JsonUtility.FromJson<MissileLocationData>(message);
        Vector3 newLocation = JsonUtility.FromJson<Vector3>(message);
        transform.position = data.position;
        transform.eulerAngles = data.rotation;
    }
}
/*
public class MissleMessage
{
    public string clientID;
    public Vector3 location;
    public bool isBoom;
}
*/