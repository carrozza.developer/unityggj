﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public float timeLimit = 120.0f;
    public Generator[] generators;
    public Pyramid pyramid;
    public Transform playCanvas;
    public Transform endGameCanvas;
    public GameObject winnerPrompt, loserPrompt;
    private StateManager sm;

    public Text timeLabel;

    private bool isPlaying, isPostReport;

    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        sm = (StateManager)FindObjectOfType(typeof(StateManager));
    }
    private float gameTime = 0;
    // Update is called once per frame
    void Update()
    {
        
            if (isPlaying)
        {
            gameTime += Time.deltaTime;
             // Check if pyramid is down....
             if (pyramid.isDestroied || gameTime >= timeLimit)
            {
                EndGame();
            }

            timeLabel.text = Mathf.RoundToInt(timeLimit - gameTime).ToString();
        } else
        {
            if (!isPostReport)
            {
                playCanvas.gameObject.SetActive(true);
                if (Input.GetButton("Fire3"))
                {
                    StartGame();
                }
            }else
            {
                Debug.Log("Waiting");
                if (Input.GetButton("Fire3"))
                {
                    RestartGame();
                }
            }
        }
    }

    public void StartGame()
    {
        for (int i = 0; i < generators.Length; i++)
        {
            generators[i].health = 100;
        }
        pyramid.isDestroied = false;
        playCanvas.gameObject.SetActive(false);
        gameTime = 0;
        isPlaying = true;
        sm.FallWhenReady(true);
    }

    private void RestartGame()
    {
        endGameCanvas.gameObject.SetActive(false);

        isPlaying = false;
        isPostReport = false;

    }

    public void EndGame()
    {
        isPlaying = false;
        isPostReport = true;
        sm.FallWhenReady(false);
        endGameCanvas.gameObject.SetActive(true);
        if (isWinner())
        {
            winnerPrompt.SetActive(true);
            loserPrompt.SetActive(false);
        } else
        {
            loserPrompt.SetActive(true);
            winnerPrompt.SetActive(false);
        }


    }

    private bool isWinner()
    {
        if (pyramid.isDestroied)
        {
            return false;
        }
        for (int i = 0; i < generators.Length; i++)
        {
            if (generators[i].health > 0)
            {
                return true;
            }
        }
        return false;
    }
}
