﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : MonoBehaviour
{
    public bool isRepairing;
    public Generator currentGenerator;
    public float fixRate = 0.3f;
    public Animator animator;
    // Update is called once per frame
    void Update()
    {
        if (currentGenerator != null && Input.GetButton("Fire1"))
        {
            currentGenerator.health += Time.deltaTime * fixRate;

            animator.SetFloat("Blend", 1.0f);

        } else
        {
            animator.SetFloat("Blend", 0.0f);
        }
    }
}
