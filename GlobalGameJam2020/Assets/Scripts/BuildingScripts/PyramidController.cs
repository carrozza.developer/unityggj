﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PyramidController : MonoBehaviour
{
    public Transform sataliteDish;

    private Transform target = null;
    private List<Transform> targetList = new List<Transform>();

    private Vector3 startingRotation = new Vector3(0, -15, 0);
    private Animation animation;
    public Vector3 offset;
    private void Start()
    {
        animation = GetComponent<Animation>();
    }
    private void Update()
    {
        if (animation.isPlaying)
        {
            
        }
        else
        {
          
            if (target)
            {
                //Debug.Log("Looking at");
                sataliteDish.LookAt(target);
                // Start Killing....
                // TODO: KILL MISSLES.
            }
            else
            {
                if (targetList.Count > 0)
                {
                    target = targetList[0];
                }
                else
                {
                   // Debug.Log("Clear");
                    target = null;
                   // sataliteDish.rotation = Quaternion.Euler(startingRotation);
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("target"))
        {
            if (target != null)
            {
                target = other.transform;
                targetList.Add(other.transform);

            } else
            {
                targetList.Add(other.transform);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("target"))
        {
            // Debug.Log("Remove");
            if (targetList.Contains(other.transform))
            {
                targetList.Remove(other.transform);
            }
            target = null;
        }
    }

}
