﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissleNavigator : MonoBehaviour
{
    public float xInput;
    public float yInput;
    public bool readyFall;
    public static bool canFall = false;
    float rotationSpeeed = 20;
    public float power = 10f;
    private ParticleSystem ps;

    private Vector3 startLocation;
    private Vector3 startRotation;

    void Start()
    {
        ps = GetComponentInChildren<ParticleSystem>();
        startLocation = transform.position;
        startRotation = transform.eulerAngles;
    }

    public void resetPosition()
    {
        transform.position = startLocation;
        transform.eulerAngles = startRotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (canFall && readyFall)
        {
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            // COmment this out when its getting set from outside
            // xInput = Input.GetAxis("Horizontal");
            // yInput = Input.GetAxis("Vertical");
            // ************************************************* //


            Vector3 heading = new Vector3(yInput * Time.deltaTime * rotationSpeeed, xInput * Time.deltaTime * rotationSpeeed, 0);
            transform.position += transform.forward * Time.deltaTime * power;


            // CharacterController controller = GetComponent<CharacterController>();
            transform.Rotate(heading);
            // Rotate around y - axis
            //rig.AddForce(Vector3.forward * Input.GetAxis("Horizontal") * navSpeed, ForceMode.Acceleration);

            // Move forward / backward
            // Vector3 forward = transform.TransformDirection(Vector3.forward);
            //  float curSpeed = speed * Input.GetAxis("Vertical");
            // controller.SimpleMove(forward * curSpeed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        GameObject boom = Instantiate(ps.gameObject, ps.transform.position, ps.transform.rotation);
        boom.transform.SetParent(null);
       // Destroy(gameObject);

        boom.GetComponent<ParticleSystem>().Play();
        //boom.GetComponent<AudioSource>().Play();
        
        resetPosition();
        ////TODO: DROP THE PLAYERS FLAG WITH BOOOM
        //readyFall = false;
        //GetComponent<Missile>().Exploded = true;
        //resetPosition();
        //gameObject.SetActive(false);

        //boom.GetComponent<ParticleSystem>().Play();
        //boom.GetComponent<AudioSource>().Play();
    }
    
}
