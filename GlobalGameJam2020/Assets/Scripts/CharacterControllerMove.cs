﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CharacterControllerMove : MonoBehaviour
{
    public float speed = 6.0f;
    public float rotateSpeed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
    private Vector3 startPostion;
    private void Start()
    {
        controller = GetComponent<CharacterController>();
        startPostion = transform.position;
    }
    void Update()
    {
        if (controller.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = transform.TransformDirection(Vector3.forward);
            moveDirection *= Input.GetAxis("Vertical") * speed;
            transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
            if (Input.GetButton("Fire2"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        controller.Move(moveDirection * Time.deltaTime);

        /*
        // Rotate around y - axis
        
        // Debug.Log(Input.GetAxis("Horizontal"));
        if (Input.GetButton("Jump") && controller.isGrounded)
        {
            controller.Move(Vector3.up);
        }
        // Move forward / backward
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        float curSpeed = speed * Input.GetAxis("Vertical");
        controller.SimpleMove(forward * curSpeed);*/
    }

    public void ResetPlayer()
    {
        transform.position = startPostion;
    }
}

