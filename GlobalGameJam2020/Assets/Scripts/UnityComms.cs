﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class UnityComms : MonoBehaviour
{
    public Text debugText;
    public void Messenger(string objectString) {
        debugText.text = objectString;
    }

     [DllImport("__Internal")]
    private static extern new void SendMessage(string message);
    public void sendMessage(string message) {
        SendMessage(message);
    }
}
