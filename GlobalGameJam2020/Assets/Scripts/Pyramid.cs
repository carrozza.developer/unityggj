﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pyramid : MonoBehaviour
{
    private StateManager sm;
    public bool isDestroied = false;
    public bool isValunerable;
    private GameManager gm;
    private Generator[] gens;
    public GameObject shield;
    // Start is called before the first frame update4444444
    void Start()
    {
        gm = (GameManager)FindObjectOfType(typeof(GameManager));
        gens = gm.generators;
        sm = (StateManager)FindObjectOfType(typeof(StateManager));
        sm.subscribe(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        bool isSafe = false;
        for (int i = 0; i < gens.Length; i++)
        {
            if (gens[i].health > 0)
            {
                isSafe = true;
                continue;
            }
        }
        if (!isSafe)
        {
            shield.SetActive(false);
            isValunerable = true;
        }else
        {
            shield.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("target") && isValunerable)
        {
            isDestroied = true;
        }
    }
    public ObjectState getStateInfo()
    {
        return new ObjectState();
    }
}
public class ObjectState
{
    string UID;
}